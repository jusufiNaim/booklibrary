package com.example.jusuf.booklibrary.remote;

import com.example.jusuf.booklibrary.model.Book;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by jusuf on 10.3.2018.
 */

public interface OpenLibrary {
    @GET("search.json")
    Call<Book> getBooks(@QueryMap Map<String, String> params);

}
