package com.example.jusuf.booklibrary.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
// TODO: Convert lists to Strings to make the app faster;

public class Doc implements Parcelable {


    public static final Creator<Doc> CREATOR = new Creator<Doc>() {
        @Override
        public Doc createFromParcel(Parcel in) {
            return new Doc(in);
        }

        @Override
        public Doc[] newArray(int size) {
            return new Doc[size];
        }
    };
    @SerializedName("isbn")
    @Expose
    private List<String> isbn = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("author_name")
    @Expose
    private List<String> authorName = null;
    @SerializedName("first_publish_year")
    @Expose
    private int firstPublishYear;
    @SerializedName("edition_count")
    @Expose
    private int editionCount;
    @SerializedName("publisher")
    @Expose
    private List<String> publisher = null;

    @SerializedName("cover_i")
    @Expose
    private int coverId;

    public Doc() {
    }


    /**
     * @param title
     * @param editionCount
     * @param isbn
     * @param firstPublishYear
     * @param authorName
     * @param publisher
     * @param coverId
     */

    public Doc(List<String> isbn, String title, List<String> authorName, int firstPublishYear, int editionCount, List<String> publisher, int coverId) {
        this.isbn = isbn;
        this.title = title;
        this.authorName = authorName;
        this.firstPublishYear = firstPublishYear;
        this.editionCount = editionCount;
        this.publisher = publisher;
        this.coverId = coverId;
    }

    protected Doc(Parcel in) {
        title = in.readString();
        editionCount = in.readInt();
        isbn = in.readArrayList(String.class.getClassLoader());
        firstPublishYear = in.readInt();
        authorName = in.readArrayList(String.class.getClassLoader());
        publisher = in.readArrayList(String.class.getClassLoader());
        coverId = in.readInt();
    }

    public List<String> getIsbn() {
        return isbn;
    }

    public void setIsbn(List<String> isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAuthorName() {
        return authorName;
    }

    public void setAuthorName(List<String> authorName) {
        this.authorName = authorName;
    }

    public int getFirstPublishYear() {
        return firstPublishYear;
    }

    public void setFirstPublishYear(int firstPublishYear) {
        this.firstPublishYear = firstPublishYear;
    }

    public int getEditionCount() {
        return editionCount;
    }

    public void setEditionCount(int editionCount) {
        this.editionCount = editionCount;
    }

    public List<String> getPublisher() {
        return publisher;
    }

    public void setPublisher(List<String> publisher) {
        this.publisher = publisher;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getCoverId() {
        return coverId;
    }

    public void setCoverId(int coverId) {
        this.coverId = coverId;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeInt(editionCount);
        dest.writeList(isbn);
        dest.writeInt(firstPublishYear);
        dest.writeList(authorName);
        dest.writeList(publisher);
        dest.writeInt(coverId);
    }
}