
package com.example.jusuf.booklibrary.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Book {

    @SerializedName("start")
    @Expose
    private int start;
    @SerializedName("numFound")
    @Expose
    private int numFound;
    @SerializedName("docs")
    @Expose
    private List<Doc> docs = null;

    /**
     * No args constructor for use in serialization
     */
    public Book() {
    }

    /**
     * @param start
     * @param docs
     * @param numFound
     */
    public Book(int start, int numFound, List<Doc> docs) {
        super();
        this.start = start;
        this.numFound = numFound;
        this.docs = docs;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getNumFound() {
        return numFound;
    }

    public void setNumFound(int numFound) {
        this.numFound = numFound;
    }

    public List<Doc> getDocs() {
        return docs;
    }

    public void setDocs(List<Doc> docs) {
        this.docs = docs;
    }

}