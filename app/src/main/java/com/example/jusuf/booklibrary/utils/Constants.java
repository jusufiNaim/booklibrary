package com.example.jusuf.booklibrary.utils;

/**
 * Created by jusuf on 11.3.2018.
 */

public class Constants {
    public static final String COVER_URL = "http://covers.openlibrary.org/b/%s/%s-M.jpg?default=false";
    public static final String COVER_ISBN_PARAM = "isbn";
    public static final String COVER_ID_PARAM = "id";
}
