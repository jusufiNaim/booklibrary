package com.example.jusuf.booklibrary.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jusuf.booklibrary.R;
import com.example.jusuf.booklibrary.model.Doc;
import com.example.jusuf.booklibrary.ui.BookActivity;

import java.util.List;

/**
 * Created by jusuf on 10.3.2018.
 */

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.CustomView> {
    private List<Doc> mDataSet;
    private Context mContext;

    public BooksAdapter(Context context, List<Doc> dataSet) {
        mDataSet = dataSet;
        mContext = context;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_list_item, parent, false);
        return new BooksAdapter.CustomView(view);
    }

    @Override
    public void onBindViewHolder(CustomView holder, int position) {
        final Doc doc = mDataSet.get(position);
        String author = "";
        if (doc.getAuthorName() != null)
            author = TextUtils.join(",", doc.getAuthorName());
        holder.mTitle.setText(String.format(mContext.getString(R.string.title), doc.getTitle()));
        holder.mAuthor.setText(String.format(mContext.getString(R.string.author), author));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(BookActivity.newIntent(mContext, doc));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    class CustomView extends RecyclerView.ViewHolder {

        TextView mTitle, mAuthor;

        private CustomView(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.text_book_title);
            mAuthor = itemView.findViewById(R.id.text_book_author);
        }
    }
}
