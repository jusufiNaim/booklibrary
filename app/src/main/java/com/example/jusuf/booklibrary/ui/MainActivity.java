package com.example.jusuf.booklibrary.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jusuf.booklibrary.R;
import com.example.jusuf.booklibrary.adapter.BooksAdapter;
import com.example.jusuf.booklibrary.model.Book;
import com.example.jusuf.booklibrary.model.Doc;
import com.example.jusuf.booklibrary.remote.ApiClient;
import com.example.jusuf.booklibrary.remote.OpenLibrary;
import com.example.jusuf.booklibrary.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    public static final String KEY_SAVED_INSTANCE = MainActivity.class.getSimpleName() + "_key";
    List<Doc> mDocList = new ArrayList<>();
    BooksAdapter mAdapter;
    RecyclerView mRecyclerView;
    EditText mSearchEditText;
    RadioButton mAuthorRadio, mTitleRadio;
    RadioGroup mRadioGroup;
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        Initialize Views
        mRecyclerView = findViewById(R.id.recycler_books);
        mSearchEditText = findViewById(R.id.search_bar);
        mAuthorRadio = findViewById(R.id.radio_author);
        mTitleRadio = findViewById(R.id.radio_title);
        mRadioGroup = findViewById(R.id.radio_group);
        mProgressBar = findViewById(R.id.progress);

        mSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_NULL) {
                    makeApiCall();
                }
                return true;
            }
        });

        ImageButton button = findViewById(R.id.search_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeApiCall();
            }
        });

        if (savedInstanceState != null) {
            mDocList = savedInstanceState.getParcelableArrayList(KEY_SAVED_INSTANCE);
            updateUi();
        }
    }

    private void makeApiCall() {
//        hide soft keyboard
        Utils.hideKeyboard(this);
//        check for internet connection
        if (!Utils.isConnected(this)) {
            showInternetNotAvailable(this);
            return;
        }
//      check for empty Search Bar
        if (Utils.getSearchText(mSearchEditText).equalsIgnoreCase("")) {
            mSearchEditText.setError("Please enter search parameters");
            mSearchEditText.requestFocus();
            return;
        }
//      show ProgressBar and clear adapter from previous enteries
        Utils.showProgressBar(mProgressBar);
        mDocList.clear();
        updateUi();

//      get the criteria to construct the URL
        String param = getSearchParameter();
        String query = Utils.getSearchText(mSearchEditText);

//      Make the api call
        OpenLibrary openLibrary = ApiClient.getClient().create(OpenLibrary.class);
        Map<String, String> map = new HashMap<>();
        map.put(param, query);
        Call<Book> bookCall = openLibrary.getBooks(map);
        bookCall.enqueue(new Callback<Book>() {
            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {
                Utils.hideProgressBar(mProgressBar);
                if (response.isSuccessful() && (response.body().getDocs() != null)) {
                    if (response.body().getDocs().size() > 0) {
                        Book book = response.body();
                        mDocList = book.getDocs();
                        updateUi();
                    } else
                        Toast.makeText(MainActivity.this, getString(R.string.message_no_results), Toast.LENGTH_LONG).show();
                } else if (response.code() == 500) {
                    Toast.makeText(MainActivity.this, getString(R.string.message_no_results), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Book> call, Throwable t) {
                mDocList.clear();
                Utils.hideProgressBar(mProgressBar);
                Toast.makeText(MainActivity.this, getString(R.string.message_no_results), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(KEY_SAVED_INSTANCE, (ArrayList<Doc>) mDocList);
    }

    /**
     * update the User Interface based on the results that are stored in ArrayList mDocList
     */
    private void updateUi() {
        mAdapter = new BooksAdapter(this, mDocList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    /**
     * @return String containing the text of the checked radioButton
     */
    private String getSearchParameter() {
        String param = "";
        switch (mRadioGroup.getCheckedRadioButtonId()) {
            case R.id.radio_author:
                param = mAuthorRadio.getText().toString();
                break;
            case R.id.radio_title:
                param = mTitleRadio.getText().toString();
                break;
        }
        return param.toLowerCase();
    }


    /**
     * Show Dialog Fragment informing the user that internet connection is not available,
     * with the option to enable wifi
     *
     * @param context
     */
    void showInternetNotAvailable(Context context) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(getString(R.string.no_internet));
        dialog.setPositiveButton(R.string.enable_wifi, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
            }
        });
        dialog.setNegativeButton(R.string.retry, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                makeApiCall();
            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }
}
