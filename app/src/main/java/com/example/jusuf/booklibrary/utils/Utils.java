package com.example.jusuf.booklibrary.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.jusuf.booklibrary.model.Doc;

import java.util.List;

import static com.example.jusuf.booklibrary.utils.Constants.COVER_ID_PARAM;
import static com.example.jusuf.booklibrary.utils.Constants.COVER_ISBN_PARAM;
import static com.example.jusuf.booklibrary.utils.Constants.COVER_URL;

/**
 * Created by jusuf on 11.3.2018.
 */

public class Utils {

    /**
     * Use this method to handle: cannot use join on null reference
     *
     * @param list
     * @return String joined from array with <,> delimiter or empty String if @param is null
     */
    public static String convertToStringWithComma(List<String> list) {
        String s = "";
        if (list != null) s = TextUtils.join(",", list);
        return s;
    }

    /**
     * Use this method to handle: cannot use join on null reference
     *
     * @param list
     * @return String joined from arrayList with new Lined delimiter or empty String if @param is null
     */
    public static String convertToStringInNewLine(List<String> list) {
        String s = "";
        if (list != null) s = TextUtils.join(System.lineSeparator(), list);
        return s;
    }

    /**
     * @param string
     * @return string or empty String if @param is null
     */
    public static String getText(String string) {
        String s = "";
        if (string != null) s = string;
        return s;
    }

    /**
     * Hide the Soft Keyboard
     *
     * @param activity
     */
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Check for internet connection
     *
     * @param context
     * @return true if available, false if not
     */
    public static boolean isConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    /**
     * Get the Text from a textview
     *
     * @param view which contains the text
     * @return lower case string containing the value of TextView
     */
    public static String getSearchText(TextView view) {
        return view.getText().toString().toLowerCase();
    }

    /**
     * Show progress bar if it's gone
     *
     * @param progressBar
     */
    public static void showProgressBar(ProgressBar progressBar) {
        if (progressBar.getVisibility() == View.GONE) progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hide the progressbar if it's visible.
     *
     * @param progressBar
     */
    public static void hideProgressBar(ProgressBar progressBar) {
        if (progressBar.getVisibility() == View.VISIBLE) progressBar.setVisibility(View.GONE);
    }

    /**
     * Construct URL to call for getting images
     *
     * @param doc whos Cover image we want downloaded
     * @return string url which we need to call
     */
    public static String constructUrl(Doc doc) {
        String url = "";
        if (doc.getCoverId() != 0) {
            url = String.format(COVER_URL, COVER_ID_PARAM, doc.getCoverId());
        } else {
            if (doc.getIsbn() != null) {
                if (!doc.getIsbn().isEmpty()) {
                    url = String.format(COVER_URL, COVER_ISBN_PARAM, doc.getIsbn().get(0));
                }
            }
        }
        return url;
    }
}
