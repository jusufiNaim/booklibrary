package com.example.jusuf.booklibrary.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jusuf.booklibrary.R;
import com.example.jusuf.booklibrary.model.Doc;
import com.example.jusuf.booklibrary.utils.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class BookActivity extends Activity {

    public static final String KEY_BOOK_ID = BookActivity.class.getSimpleName() + "_key";
    private Doc mDoc;
    private ImageView mImageView;

    public static Intent newIntent(Context context, Doc doc) {
        Intent intent = new Intent(context, BookActivity.class);
        intent.putExtra(KEY_BOOK_ID, doc);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        if (getIntent() != null)
            if (getIntent().hasExtra(KEY_BOOK_ID))
                mDoc = getIntent().getParcelableExtra(KEY_BOOK_ID);

        TextView titleTv, authorTv, publishYearTv, publisherTv, editionsCountTv, isbnTv;

        mImageView = findViewById(R.id.image_book);

        titleTv = findViewById(R.id.text_detail_title);
        authorTv = findViewById(R.id.text_detail_author);
        publishYearTv = findViewById(R.id.text_detail_publish_year);
        publisherTv = findViewById(R.id.text_detail_publisher);
        editionsCountTv = findViewById(R.id.text_detail_editions);
        isbnTv = findViewById(R.id.text_detail_isbn);

        String s = Utils.getText(mDoc.getTitle());
        titleTv.setText(String.format(getString(R.string.title), s));

        s = Utils.convertToStringInNewLine(mDoc.getAuthorName());
        authorTv.setText(String.format(getString(R.string.author), s));

        publishYearTv.setText(String.format(getString(R.string.publish_year), mDoc.getFirstPublishYear()));

        s = Utils.convertToStringInNewLine(mDoc.getPublisher());
        publisherTv.setText(String.format(getString(R.string.publisher), s));


        editionsCountTv.setText(String.format(getString(R.string.editions_count), mDoc.getEditionCount()));

        s = Utils.convertToStringInNewLine(mDoc.getIsbn());
        isbnTv.setText(String.format(getString(R.string.isbn), s));

        if (!Utils.isConnected(this)) {
            showInternetNotAvailable(this);
        } else {
            attemptToGetImage();
        }

    }

    /**
     * Making the Rest call ready
     */
    public void attemptToGetImage() {
        String url = Utils.constructUrl(mDoc);

        if (url.equals("")) {
            Toast.makeText(this, R.string.message_no_image, Toast.LENGTH_LONG).show();
            return;
        }
        new GetImageTask().execute(url);

    }

    void showInternetNotAvailable(Context context) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(getString(R.string.no_internet));
        dialog.setMessage(getString(R.string.message_internet_images));
        dialog.setPositiveButton(R.string.enable_wifi, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
            }
        });
        dialog.setNegativeButton(R.string.retry, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                attemptToGetImage();
            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    private class GetImageTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... strings) {
            String url = strings[0];
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap == null || bitmap.getByteCount() <= 1) {
                Toast.makeText(BookActivity.this, getString(R.string.message_no_image), Toast.LENGTH_LONG).show();
            } else {
                mImageView.setImageBitmap(bitmap);
            }
        }
    }
}